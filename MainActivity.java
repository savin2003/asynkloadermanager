package asavin.asynkloader;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String>{
    static ProgressBar progressBar;
    static ProgressBar progressBar2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        Bundle bndl = new Bundle();
        Loader loader1 = (Loader) getLoaderManager().initLoader(1, bndl, this);
        loader1.setpBar(progressBar);
        loader1.forceLoad();
        Loader loader2 = (Loader) getLoaderManager().initLoader(2, bndl, this);
        loader2.setpBar(progressBar2);
        loader2.forceLoad();
    }

    @Override
    public android.content.Loader<String> onCreateLoader(int id, Bundle args) {
        Loader loader = new Loader(this);
        return loader;
    }

    @Override
    public void onLoadFinished(android.content.Loader<String> loader, String data) {

    }

    @Override
    public void onLoaderReset(android.content.Loader<String> loader) {

    }


    static class Loader extends AsyncTaskLoader<String> {
        ProgressBar pBar;
        public Loader(Context context) {
            super(context);
        }
        void setpBar(ProgressBar pBar)
    {
            this.pBar = pBar;
        }
        @Override
        public String loadInBackground() {
            for (int i = 0; i < 100; i++) {
                try {

                    Thread.sleep(new Random(System.currentTimeMillis()).nextInt(50)+50);
                    pBar.setProgress(i);


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }
    }
}
